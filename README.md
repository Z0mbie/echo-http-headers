# Table of Contents

- [Table of Contents](#table-of-contents)
  - [Description](#description)
  - [Install](#install)
    - [Good Ole Hard Way](#good-ole-hard-way)
    - [RPM](#rpm)
  - [Usage](#usage)
  - [Extending](#extending)

## Description

This is a simple python3 script utilizing flask to generate a dyanmic HTML page
to show you the HTTP request headers as seen by this application. I found the
need for this simple web-app when trying to verify the HTTP requests at work
after they had progressed through several reverse-proxies, each adding
additional `X-` headers. This is much simpler than trying to remote onto the
machine and look at the logs or use some other metrics/log system to view these.

I hope you find it useful.

## Install

### Good Ole Hard Way

Pull down the script and install `python3-flask` and you're good to go

### RPM

I have provided a spec file in the root of this repo and configured tito.

This will work for both RedHat/CentOS 7 and 8

```bash
git clone https://gitlab.com/Z0mbie/echo-http-headers.git
cd echo-http-headers
sudo yum install -y tito
sudo yum-builddep -y python-echo-http-headers.spec
tito build --test --rpm
# Now you have an rpm!
yum install <new rpm>
```

## Usage

1. Run the echo headers script on the destination web app
```bash
# From script
python3 echo_http_headers.py --host 0.0.0.0 -p 8080
# From RPM
echo-http-headers --host 0.0.0.0 -p 8080
```
1. Launch a browser and hit the page
```bash
firefox <host>:<port>
```
<h1>
  <table style="width:100%">
    <tr>
      <th>Header</th>
      <th>Content</th>
    </tr>
    <tr>
        <td>Host</td>
        <td>192.168.1.31:8080</td>
        </tr><tr>
        <td>User-Agent</td>
        <td>curl/7.81.0</td>
        </tr><tr>
        <td>Accept</td>
        <td>*/*</td>
        </tr>
  </table>
</h1>

1. Test with curl
```bash
curl -H "X-New-Header: testing123" <host>:<port>
```
<h1>
  <table style="width:100%">
    <tr>
      <th>Header</th>
      <th>Content</th>
    </tr>
    <tr>
        <td>Host</td>
        <td>192.168.1.31:8080</td>
        </tr><tr>
        <td>User-Agent</td>
        <td>curl/7.81.0</td>
        </tr><tr>
        <td>Accept</td>
        <td>*/*</td>
        </tr><tr>
        <td>X-New-Header</td>
        <td>testing123</td>
        </tr>
  </table>
</h1>

## Extending

1. This project could easily be extended to provide the output as plain text or
   in application/json format for automated testing purposes
1. This can easily be placed behind an nginx reverse-proxy setup in order to
   handle TLS/SSL and redirect to basic HTTP without having to modify the
   echo_http_headers.py script itself
