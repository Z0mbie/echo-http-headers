%global modname echo-http-headers
%global _script %(echo %{modname} | tr - _)
%global _sum display http request headers as seen by the web app
%global _desc Display HTTP request headers as seen by the web app

%define debug_package %{nil}

Name:		python-%{modname}
Version:	1.0.0
Release:	1%{?dist}
Summary:	%{_sum}

Group:		Applications
License:	GNU GPLv3
URL:		https://gitlab.com/Z0mbie/echo-http-headers
Source0:	%{modname}-%{version}.tar.gz

%description
%{_desc}

%package -n python%{python3_pkgversion}-%{modname}
Summary:	%{_sum}
BuildRequires:	python%{python3_pkgversion}
BuildRequires:	python%{python3_pkgversion}-rpm-macros
BuildRequires:	python%{python3_pkgversion}-setuptools
BuildRequires:	python-rpm-macros
BuildRequires:	python-srpm-macros
Requires:	python%{python3_pkgversion}
Requires:	python%{python3_pkgversion}-flask

%description -n python%{python3_pkgversion}-%{modname}
%{_desc}

%prep
%autosetup -p1 -n %{modname}-%{version}

%build

%install
%{__rm} -rf %{buildroot}
%{__mkdir_p} %{buildroot}%{_defaultlicensedir}/%{modname}/
%{__mkdir_p} %{buildroot}%{_bindir}
%{__install} -m 0755 %{_script}.py %{buildroot}%{_bindir}/%{modname}
%{__install} -m 0644 LICENSE %{buildroot}%{_defaultlicensedir}/%{modname}/

%clean
%{__rm} -rf %{buildroot}

%files

%files -n python%{python3_pkgversion}-%{modname}
%defattr(-,root,root,-)
%license %{_defaultlicensedir}/%{modname}/LICENSE
%doc README.md
%{_bindir}/%{modname}

%changelog

