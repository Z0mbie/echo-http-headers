#!/usr/bin/env python3

"""Simple python/flask script to generate an html page displaying
    the HTTP request headers as seen by this web app
"""

import os
import logging
import sys
from argparse import ArgumentParser
from flask import Flask, render_template, request

TEMPLATE = """<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Proxy Testing</title>
<style>
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}
</style>
</head>
<body>
<h1>
  <table style="width:100%">
    <tr>
      <th>Header</th>
      <th>Content</th>
    </tr>
    %%REPLACE_ME%%
  </table>
</h1>
</body>
</html>
"""
def main():
    """Main
    """
    logging.basicConfig(level=logging.INFO)
    parser = ArgumentParser()
    parser.add_argument('-p', '--port', action='store', default=8080, type=int, help='port to listen on')
    parser.add_argument('--host', action='store', default='127.0.0.1', help='host/ip to listen on')
    parser.add_argument('-t', '--template', action='store', default='./templates',
                        help='flask template dir')
    args = parser.parse_args()

    if not os.path.isdir(args.template):
        try:
            os.makedirs(args.template, exist_ok=True)
        except OSError as error:
            logging.error(f"could not create {args.template}")  # pylint: disable=logging-fstring-interpolation
            logging.critical(error)
            sys.exit(1)

    app = Flask(__name__, template_folder=args.template)

    @app.route('/', defaults={'path': ''}, methods=['GET', 'POST'])
    @app.route('/<path:path>')
    def catch_all(path):  # pylint: disable=unused-variable
        """Generate dynamic index.html page
        """
        table_content = ""
        for header in dict(sorted(request.headers)).items():
            table_line = """<tr>
        <td>""" + header[0] + """</td>
        <td>""" + header[1] + """</td>
        </tr>"""
            table_content += table_line
        logging.info('building index.html with request headers')
        content = TEMPLATE.replace('%%REPLACE_ME%%', str(table_content))
        with open(f"{args.template}/index.html", "w", encoding="utf-8") as output:
            output.write(content)
        output.close()

        return render_template("index.html")

    app.run(debug=True, host=args.host, port=args.port)

if __name__ == "__main__":
    main()
